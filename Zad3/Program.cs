﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsolePrinter printer1 = new DataConsolePrinter();
            VirtualProxyDataset virtualProxy1 = new VirtualProxyDataset("E:\\csvfile.csv");
            Console.WriteLine("Virtual proxy dataset:");
            printer1.PrintToConsole(virtualProxy1);
            try
            {
                User firstUser = User.GenerateUser("Mihael");
                ProtectionProxyDataset protectionProxyDatasetOne = new ProtectionProxyDataset(firstUser);
                Console.WriteLine("First user dataset:");
                printer1.PrintToConsole(protectionProxyDatasetOne);
                User secondUser = User.GenerateUser("George");
                ProtectionProxyDataset protectionProxyDatasetTwo = new ProtectionProxyDataset(secondUser);
                Console.WriteLine("Second user dataset:");
                printer1.PrintToConsole(protectionProxyDatasetTwo);
            }
            catch(Exception e)
            {
                Console.WriteLine("User isn't authorized.");
            }
        }
    }
}
