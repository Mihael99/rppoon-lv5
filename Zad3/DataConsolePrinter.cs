﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class DataConsolePrinter
    {
        public void PrintToConsole(IDataset data)
        {
            if(data.GetData() == null)
            {
                throw new Exception();
            }
            foreach (List<string> secondList in data.GetData())
            {
                foreach (String dataFile in secondList)
                {
                    Console.WriteLine(dataFile);
                }
            }
        }
    }
}
