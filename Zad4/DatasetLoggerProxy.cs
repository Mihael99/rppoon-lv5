﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class DatasetLoggerProxy: IDataset
    {
        private ConsoleLogger logger;
        private string filePath;
        private Dataset dataset;
        public DatasetLoggerProxy(ConsoleLogger Logger, string filepath)
        {
            this.logger = Logger;
            this.filePath = filepath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            logger.Log();
            return dataset.GetData();
        }

    }
}
