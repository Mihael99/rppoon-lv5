﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        private string Message;
        private DateTime Time;
        private ConsoleLogger(string message)
        {
            this.Message = message;
            this.Time = DateTime.Now;
        }

        public static ConsoleLogger GetInstance(string message) 
        {
            if (instance == null)
                instance = new ConsoleLogger(message);
            return instance;
        }
        public void Log()
        {
            Console.WriteLine(this.Message + this.Time);
        }
    }
}
