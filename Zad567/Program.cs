﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 5.

            LightTheme lightTheme = new LightTheme();
            ReminderNote note1 = new ReminderNote("You need to upload your homework until 23:00PM.", lightTheme);
            note1.Show();
            SunTheme sunTheme = new SunTheme();
            Note note2 = new ReminderNote("You passed the test.", sunTheme);
            note2.Show();

            //Zadatak 6.

            GroupNote note3 = new GroupNote("This is a group note.", sunTheme);
            note3.Show();
            note3.AddToGroup("John");
            note3.AddToGroup("Jennifer");
            note3.AddToGroup("Leslie");
            note3.AddToGroup("Ron");
            note3.Show();
            GroupNote note4 = new GroupNote("This is a second group note.", lightTheme);
            note4.AddToGroup("Mark");
            note4.AddToGroup("Jim");
            note4.AddToGroup("Timothy");
            note4.Show();

            //Zadatak 7.

            Console.WriteLine("First notebook");
            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            notebook.Display();

            Console.WriteLine("Changing theme of first notebook");
            notebook.ChangeTheme(sunTheme);
            notebook.Display();

            Console.WriteLine("Second notebook");
            Notebook notebook2 = new Notebook(lightTheme);
            notebook2.AddNote(note1);
            notebook2.AddNote(note2);
            notebook2.AddNote(note3);
            notebook2.Display();

        }
    }
}
