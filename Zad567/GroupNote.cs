﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    class GroupNote: Note
    {
        List<string> GroupParticipants;
        public GroupNote(string message, ITheme theme) : base(message, theme) 
        {
            this.GroupParticipants = new List<string>();
        }
        public void RemoveFromGroup(string name)
        {
            this.GroupParticipants.Remove(name);
        }
        public void AddToGroup(string name)
        {
            this.GroupParticipants.Add(name);
        }
        public override void Show() 
        {
            this.ChangeColor();
            Console.WriteLine("Group Participants: "); 
            foreach(string name in this.GroupParticipants)
            {
                Console.WriteLine(name);
            }
            string framedMessage = this.GetFramedMessage(); 
            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
    }
}
