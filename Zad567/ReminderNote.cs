﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    class ReminderNote: Note
    {
        public ReminderNote(string message, ITheme theme) : base(message, theme) { }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER: ");
            string FramedMessage = this.GetFramedMessage();
            Console.WriteLine(FramedMessage);
            Console.ResetColor();
        }
    }
}
