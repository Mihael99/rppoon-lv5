﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad567
{
    class Notebook
    {
        ITheme theme;
        private List<Note> notes;
        public Notebook() { this.notes = new List<Note>(); }
        public Notebook(ITheme theme) 
        { 
            this.notes = new List<Note>();
            this.theme = theme;
        }
        public void AddNote(Note note) 
        { 
            this.notes.Add(note);
            if (this.theme != null)
            {
                note.Theme = this.theme;
            }
        }
        public void ChangeTheme(ITheme theme) 
        { 
            foreach (Note note in this.notes) 
            { 
                note.Theme = theme; 
            } 
        }
        public void Display()
        { 
            foreach (Note note in this.notes)
            { 
                note.Show();
                Console.WriteLine("\n");
            }
        }
    }
}
