﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1i2
{
    class ShippingService
    {
        private double WeightPrice;
        ShippingService(double weightPrice)
        {
            this.WeightPrice = weightPrice;
        }
        public void setWeightPrice(double price)
        {
            this.WeightPrice = price;
        }
        public double getWeightPrice()
        {

            return this.WeightPrice;
        }
        double CalculateDeliveryPrice(IShipable item)
        {
            double DeliveryPrice = WeightPrice * item.Weight;
            return DeliveryPrice;
        }
    }
}
